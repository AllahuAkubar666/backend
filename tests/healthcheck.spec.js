const request = require("supertest");

const createServer = require("../src/app");

describe("Testing health check endpoint", () => {
  it("GET /health should return HTTP status of 200 OK", async () => {
    await request(createServer()).get("/health").expect(200);
  });
});
