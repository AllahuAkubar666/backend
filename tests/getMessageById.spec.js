const request = require("supertest");
const redis = require("redis-mock");
const createServer = require("../src/app");

jest.mock("../src/app.config.json", () => ({ messageExpireTime: "1000s" }));

describe("Getting message by ID should work properly", () => {
  it("GET /message/:messageId should return a message with the proper ID", async () => {
    const app = createServer(redis.createClient());
    const requestBody = {
      text:
        "The Bulgarian dictionary of pronunciation, punctuation and punctuation",
    };
    let messageId;
    await request(app)
      .post("/message")
      .send(requestBody)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        messageId = response.body.id;
      });

    await request(app)
      .get(`/message/${messageId}`)
      .expect(200)
      .then((response) => {
        expect(response.body.id).toBe(messageId);
        expect(response.body.text).toBe(requestBody.text);
      });
  });

  it("GET /message/:messageId should return HTTP 404 with text 'Message with ID of ${messageId} not found!'", async () => {
    const app = createServer(redis.createClient());
    const messageId = 777;
    await request(app)
      .get(`/message/${messageId}`)
      .expect(404)
      .then((response) => {
        expect(response.body.error).toBe(
          `Message with ID of ${messageId} not found!`
        );
      });
  });

  it("POST /message without redis initialized should return HTTP 500 'Something went wrong'", async () => {
    const app = createServer();
    await request(app)
      .get("/message/666")
      .expect(500)
      .expect((response) => {
        expect(response.body.error).not.toBeUndefined();
        expect(response.body.error).toBe("Something went wrong");
      });
  });
});
