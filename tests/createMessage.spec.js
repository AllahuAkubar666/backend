const request = require("supertest");
const redis = require("redis-mock");
const createServer = require("../src/app");

describe("Creating new messages", () => {
  it("POST /message with json object with text field should create new message", async () => {
    const app = createServer(redis.createClient());
    const requestBody = { text: "This is aweomse" };
    await request(app)
      .post("/message")
      .send(requestBody)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.id).not.toBeUndefined();
        expect(response.body.text).toEqual(requestBody.text);
      });
  });

  it("POST /message with empty text field should return HTTP 400 with text 'Text of message missing'", async () => {
    const app = createServer(redis.createClient());
    const requestBody = {};
    await request(app)
      .post("/message")
      .send(requestBody)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(400)
      .expect((response) => {
        expect(response.body.error).not.toBeUndefined();
        expect(response.body.error).toBe("Text of message missing");
      });
  });

  it("POST /message without redis initialized should return HTTP 500 'Something went wrong'", async () => {
    const app = createServer();
    const requestBody = { text: "Opps, redis is not working!" };
    await request(app)
      .post("/message")
      .send(requestBody)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(500)
      .expect((response) => {
        expect(response.body.error).not.toBeUndefined();
        expect(response.body.error).toBe("Something went wrong");
      });
  });

  it("should call multi() set() zadd() on redis instance", async () => {
    const redisClient = redis.createClient();
    const redisMulti = jest.spyOn(redisClient, "multi");
    const redisSet = jest.spyOn(redisClient, "set");
    const redisZadd = jest.spyOn(redisClient, "zadd");
    const app = createServer(redisClient);
    const requestBody = {
      text: "The Gulag Archipelago by Alexandr Soljenitin ",
    };
    await request(app)
      .post("/message")
      .send(requestBody)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200);
    expect(redisMulti).toHaveBeenCalled();
    expect(redisSet).toHaveBeenCalled();
    expect(redisZadd).toHaveBeenCalled();
  });

  it("POST /message with unsanitized input should sanitize it in backend", async () => {
    const app = createServer(redis.createClient());
    const requestBody = {
      text: "<script>alert('You've been hacked!')</script>",
    };
    let messageId;
    await request(app)
      .post("/message")
      .send(requestBody)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        messageId = response.body.id;
      });

    await request(app)
      .get(`/message/${messageId}`)
      .expect(200)
      .expect((response) => {
        expect(response.body.text).toBe(
          "&lt;script&gt;alert(&#x27;You&#x27;ve been hacked!&#x27;)&lt;&#x2F;script&gt;"
        );
      });
  });
});
