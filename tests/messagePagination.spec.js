const request = require("supertest");
const redis = require("redis-mock");
const createServer = require("../src/app");
const { response } = require("express");

describe("Message pagination should work", () => {
  it("GET /message?startPage=0&endPage=4 should return 4 results", async () => {
    const app = createServer(redis.createClient());
    const requestBody = { text: "Only one message sent 5 times is enough" };
    for (let i = 0; i < 5; ++i) {
      await request(app).post("/message").send(requestBody).expect(200);
    }
    await request(app)
      .get("/message")
      .query({ startPage: 0, endPage: 4 })
      .expect(200)
      .expect((response) => {
        expect(response.body.length).toBe(5);
        for (let i = 0; i < 5; ++i) {
          expect(response.body[i].id).not.toBeUndefined();
          expect(response.body[i].text).toEqual(requestBody.text);
        }
      });
  });

  it("GET /message with no query parameters should return HTTP 422 error 'Invalid query parameter/s'", async () => {
    const app = createServer(redis.createClient());
    await request(app)
      .get("/message")
      .expect(422)
      .expect((response) => {
        expect(response.body.error).toBe("Invalid query parameter/s");
      });
  });

  it("GET /message?startPage=-1&endPage=4 should return HTTP 422 error 'Invalid query parameter/s'", async () => {
    const app = createServer(redis.createClient());
    await request(app)
      .get("/message")
      .query({ startPage: -1, endPage: 4 })
      .expect(422)
      .expect((response) => {
        expect(response.body.error).toBe("Invalid query parameter/s");
      });
  });

  it("GET /message?startPage=0&endPage=-1 should return HTTP 422 error 'Invalid query parameter/s'", async () => {
    const app = createServer(redis.createClient());
    await request(app)
      .get("/message")
      .query({ startPage: 0, endPage: -1 })
      .expect(422)
      .expect((response) => {
        expect(response.body.error).toBe("Invalid query parameter/s");
      });
  });

  it("GET /message?startPage=0&endPage=2 (less than 5 items) should return HTTP 400 error 'Only 5 results per pagination request are allowed to be returned'", async () => {
    const app = createServer(redis.createClient());
    await request(app)
      .get("/message")
      .query({ startPage: 0, endPage: 2 })
      .expect(400)
      .expect((response) => {
        expect(response.body.error).toBe(
          "Only 5 results per pagination request are allowed to be returned"
        );
      });
  });

  it("GET /message?startPage=zero&endPage=four (not numbers for pages) should return HTTP 422 error 'Invalid query parameter/s'", async () => {
    const app = createServer(redis.createClient());
    await request(app)
      .get("/message")
      .query({ startPage: "one", endPage: "four" })
      .expect(422)
      .expect((response) => {
        expect(response.body.error).toBe("Invalid query parameter/s");
      });
  });

  it("GET /message?startPage=0&endPage=4 without redis client should return HTTP 500 'Something went wrong'", async () => {
    const app = createServer();
    await request(app)
      .get("/message")
      .query({ startPage: 0, endPage: 4 })
      .expect(500)
      .expect((response) => {
        expect(response.body.error).toBe("Something went wrong");
      });
  });
});
