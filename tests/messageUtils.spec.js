const uuid = require("uuid");
const {
  createMessage,
  createKeyFromMessageID,
  extractMessageIdFromKey,
} = require("../src/messages/messagesUtils");

describe("Message utils should work", () => {
  it("create message should create a message with ID and text", () => {
    const text = "This is awesome!";
    const message = createMessage(text);
    expect(message.id).not.toBeUndefined();
    expect(message.text).toBe(text);
  });

  it("create key from message should return properly formatted redis key", () => {
    const messageId = uuid.v4();
    const expectedKey = `message:${messageId}`;
    const actualKey = createKeyFromMessageID(messageId);
    expect(actualKey).toBe(expectedKey);
  });

  it("should extract a valid message ID from redis key", () => {
    const messageId = uuid.v4();
    const redisKey = `message:${messageId}`;
    const extractedMessageId = extractMessageIdFromKey(redisKey);
    expect(extractedMessageId).toBe(messageId);
  });
});
