const app = require("./app");
const redis = require("redis");
const createServer = require("./app");

const PORT = process.env.PORT || 3000;

const client = redis.createClient();

client.on("error", (err) => {
  console.log("we have an error", err);
});

client.on("ready", () => {
  const app = createServer(client);
  app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
});
