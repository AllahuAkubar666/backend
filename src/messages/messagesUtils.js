const uuid = require("uuid");
const appConfig = require("../app.config.json");

const createMessage = (text) => ({
  id: uuid.v4(),
  text,
});

const createKeyFromMessageID = (messageId) => `message:${messageId}`;

const extractMessageIdFromKey = (messageKey) => {
  const MESSAGE_ID_POSITION = 1;
  return messageKey.split(":")[MESSAGE_ID_POSITION];
};

const getKeyExpiry = () => {
  let messageExpireTime = 60;
  if (appConfig.messageExpireTime) {
    let time = appConfig.messageExpireTime.split("s");
    if (time.length) {
      time = Number(time[0]);
      if (Number.isInteger(time)) {
        messageExpireTime = time;
      }
    }
  }
  return messageExpireTime;
};

module.exports = {
  createMessage,
  createKeyFromMessageID,
  extractMessageIdFromKey,
  getKeyExpiry,
};
