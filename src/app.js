const express = require("express");
const { promisify } = require("util");
const { body } = require("express-validator");
const _ = require("lodash");
const cors = require("cors");
const {
  createMessage,
  createKeyFromMessageID,
  extractMessageIdFromKey,
  getKeyExpiry,
} = require("./messages/messagesUtils");

const createServer = (redisClient) => {
  const app = express();

  app.use(express.json());
  app.use(cors());

  app.get("/health", (req, res) => res.sendStatus(200));

  app.post("/message", body("text").trim().escape(), async (req, res) => {
    const { text } = req.body;
    if (!text) {
      return res.status(400).send({
        error: "Text of message missing",
      });
    }
    const message = createMessage(text);
    const messageKey = createKeyFromMessageID(message.id);
    const messageExpireTime = getKeyExpiry();
    try {
      const zrem = promisify(redisClient.zrem).bind(redisClient);
      const multi = redisClient
        .multi()
        .set(messageKey, message.text, "EX", messageExpireTime)
        .zadd(["messages", new Date().getTime(), messageKey]);
      await promisify(multi.exec).call(multi);
      /*
                This is not the best solution for handling expired messages.
                It could crash the app (for example by blowing the heap).
                It is better to create a separate process which periodically
                deletes messages from redis
            */
      setTimeout(
        () => zrem("messages", messageKey).catch((e) => console.error(e)),
        messageExpireTime * 1000
      );
    } catch (e) {
      return res.status(500).send({
        error: "Something went wrong",
      });
    }
    res.send(message);
  });

  app.get("/message", async (req, res) => {
    const { startPage, endPage } = req.query;
    if (
      !Number.isInteger(Number(startPage)) ||
      !Number.isInteger(Number(endPage))
    ) {
      return res.status(422).send({
        error: "Invalid query parameter/s",
      });
    }
    if (startPage < 0 || endPage < 0) {
      return res.status(422).send({
        error: "Invalid query parameter/s",
      });
    }
    if (endPage - startPage + 1 !== 5) {
      return res.status(400).send({
        error:
          "Only 5 results per pagination request are allowed to be returned",
      });
    }
    try {
      const zscan = promisify(redisClient.zrange).bind(redisClient);
      const mget = promisify(redisClient.mget).bind(redisClient);
      const messageKeys = await zscan("messages", startPage, endPage);
      if (messageKeys.length) {
        const MESSAGE_KEY_POSITION = 0;
        const MESSAGE_TEXT_POSITION = 1;
        const VALID_MESSAGES_POSITION = 0;
        const messageTexts = await mget(messageKeys);
        const messages = _.zip(messageKeys, messageTexts);
        const validInvalidMessages = _.partition(
          messages,
          (message) => message[MESSAGE_TEXT_POSITION] !== null
        );
        const validMessages = validInvalidMessages[VALID_MESSAGES_POSITION].map(
          (validMessage) => ({
            id: extractMessageIdFromKey(validMessage[MESSAGE_KEY_POSITION]),
            text: validMessage[MESSAGE_TEXT_POSITION],
          })
        );
        return res.send(validMessages);
      }
      return res.status(404).send({
        error: `No messages found for in range of pages ${startPage} and ${endPage}`,
      });
    } catch (e) {
      return res.status(500).send({
        error: "Something went wrong",
      });
    }
  });

  app.get("/message/:messageId", async (req, res) => {
    const { messageId } = req.params;
    if (!messageId) {
      return res.status(400).send({
        error: "Message ID missing",
      });
    }
    const messageKey = createKeyFromMessageID(messageId);
    try {
      const mget = promisify(redisClient.mget).bind(redisClient);
      const MESSAGE_TEXT_POSITION = 0;
      const result = await mget(messageKey);
      const messageText = result[MESSAGE_TEXT_POSITION];
      if (!messageText) {
        return res.status(404).send({
          error: `Message with ID of ${messageId} not found!`,
        });
      }
      res.send({ id: messageId, text: messageText });
    } catch (e) {
      res.status(500).send({
        error: "Something went wrong",
      });
    }
  });

  return app;
};

module.exports = createServer;
